@extends('admin.app')

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('Home')</a>
</li>
<li class="breadcrumb-item"><a href="{{ route('reservations-services.index') }}">@lang('Reservations Blogs')</a>
</li>

@endsection

@section('content')
    <div class="row" id="basic-table">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('Reservations Blogs')</h4>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <a href="{{ route('reservations-services.create') }}"
                                   class="btn btn-icon btn-primary waves-effect"
                                   data-bs-toggle="tooltip"
                                   data-bs-placement="left"
                                   title="@lang('Add New')">
                                    <i data-feather='plus-square'></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>@lang('Name')</th>
                            <th>@lang('Email')</th>
                            <th>@lang('Mobile')</th>
                            <th class="width-150"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($AllData as $Data)
                            <tr>
                                <td>
                                    <span class="fw-bold">{{$Data->name}}</span>
                                </td>
                                <td>
                                    <span class="fw-bold">{{$Data->email}}</span>
                                </td>
                                <td>
                                    <span class="fw-bold">{{$Data->mobile}}</span>
                                </td>
                               
                                <td>

                                    <a href="{{route('reservations-services.show',$Data)}}" class="btn btn-icon btn-outline-primary waves-effect">
                                        <i data-feather='eye'></i>
                                    </a>


                                    <a href="{{route('reservations-services.edit',$Data)}}" class="btn btn-icon btn-outline-primary waves-effect">
                                        <i data-feather='edit'></i>
                                    </a>
                                

                                    {{ html()->form('DELETE', route('reservations-services.destroy',$Data))->class('d-inline')->open()}}
                                    <button type="submit" class="btn btn-icon btn-outline-danger waves-effect confirm">
                                        <i data-feather='trash-2'></i>
                                    </button>
                                    {{ html()->form()->close()}}
                                </td> 
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="text-center"></td>
                            </tr>
                        @endforelse
                        
                        </tbody>
                       
                    </table>
                    <div class="justify-content-center">
                        {!! $AllData->links('pagination::bootstrap-4') !!}

            
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection