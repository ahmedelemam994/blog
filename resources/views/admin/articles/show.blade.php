@extends('admin.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
     
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> @lang('View') @lang('Reservation Blog') </h4>
                    </div>
                    
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="name">@lang('View') </label>
                                    <p>{{  $ReservationService->name }}</p>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="email">@lang('Email') </label>
                                    <p>{{  $ReservationService->email }}</p>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="mobile">@lang('Mobile') </label>
                                    <p>{{  $ReservationService->mobile }}</p>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="detail">@lang('Detail') </label>
                                    <p>{{  $ReservationService->detail }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
    
        </div>
    </div>
@endsection
