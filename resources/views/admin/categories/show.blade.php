@extends('admin.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
     
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> @lang('View') @lang('Category') </h4>
                    </div>
                    
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="name">@lang('Name') </label>
                                    <p>{{ $Category->translate($key)->title ?? '' }} </p>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    
                </div>
    
        </div>
    </div>
@endsection
