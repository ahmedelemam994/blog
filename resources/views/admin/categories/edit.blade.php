@extends('admin.app')

@section('css')
   <!-- BEGIN: Page CSS-->
  <!-- END: Page CSS-->
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('Home')</a>
</li>
<li class="breadcrumb-item"><a href="{{ route('categories.index') }}">@lang('Categories')</a>
</li>
<li class="breadcrumb-item active" ><a href="{{ route('categories.create') }}" >@lang('Modify')</a>
</li>
@endsection

@section('content')
<section id="basic-tabs-components">
    <div class="row match-height">
        <!-- Basic Tabs starts -->
        <div class="col-lg-12">
            <form method="POST" action="{{ route('categories.update', $Category->id) }}">
            @csrf
            @method('PUT')
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> @lang('Modify') @lang('Category') </h4>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach (config('app.languages') as $key => $lang)
                        <li class="nav-item">
                            <a class="nav-link @if ($loop->index == 0) active @endif" 
                                id="{{ $lang }}-tab" data-bs-toggle="tab" href="#{{ $key }}" 
                                aria-controls="{{ $lang }}" role="tab" aria-selected="true">{{ $lang }}</a>
                        </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach (config('app.languages') as $key => $lang)
                        <div class="tab-pane @if ($loop->index == 0) active @endif" id="{{ $key }}" 
                        aria-labelledby="{{ $key }}-tab" role="tabpanel">
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="TitleInput">@lang('Title')</label>
                                    <input id="TitleInput"  
                                    value={{ $Category->translate($key)->title ?? '' }} name="{{$key}}[title]" class="form-control form-control-lg"
                                     type="text" placeholder="@lang('Title') {{$key}}" />
                                </div>
                            </div>
                        </div>
                            
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary waves-effect waves-float waves-light" type="submit">@lang('Modify')</button>
                </div>
            </div>
           </form>
        </div>
        <!-- Basic Tabs ends -->

    </div>
</section>
@endsection

@section('js')
<!-- BEGIN: Theme JS-->
<script src="{{asset('admin')}}/app-assets/js/scripts/components/components-navs.js"></script>
<!-- END: Theme JS-->
@endsection