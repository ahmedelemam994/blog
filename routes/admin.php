<?php

use App\Http\Controllers\Admin\ArticlesController;
use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\CustomAuthController;
use App\Http\Controllers\Admin\ReservationsServicesController;
require 'breadcrumbs.php';



Route::get('login',[CustomAuthController::class,'login'])->name('login');
Route::get('signup',[CustomAuthController::class,'signup'])->name('signup');
Route::post('postlogin',[CustomAuthController::class,'postlogin'])->name('postlogin'); 
Route::post('signupsave', [CustomAuthController::class,'signupsave'])->name('signupsave'); 
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath','auth' ]
    ], function(){
       
        Route::resource('categories', CategoriesController::class);
        Route::resource('articles', ArticlesController::class);
        Route::resource('reservations-services', ReservationsServicesController::class);
        Route::controller(CustomAuthController::class)->group(function () {
            Route::get('/','home')->name('home'); 
            Route::get('dashboard','dashboard'); 
            Route::get('signout', 'signOut')->name('signout');
            Route::get('forget-password','showForgetPasswordForm')->name('forget.password.get');
            Route::post('forget-password', 'submitForgetPasswordForm')->name('forget.password.post'); 
            Route::get('reset-password/{token}','showResetPasswordForm')->name('reset.password.get');
            Route::post('reset-password','submitResetPasswordForm')->name('reset.password.post');
        });
        
    });


// Route::group(['prefix' => 'dashboard', 'as' => 'dashboard.', 'middleware' => ['auth', 'checkLogin']], function () {

//     Route::get('/', function () {
//         return view('dashboard.layouts.layout');
//     })->name('index');


//     Route::get('/settings', [SettingController::class, 'index'])->name('settings');

//     Route::post('/settings/update/{setting}', [SettingController::class, 'update'])->name('settings.update');


//     Route::get('/users/all', [UserController::class, 'getUsersDatatable'])->name('users.all');
//     Route::post('/users/delete', [UserController::class, 'delete'])->name('users.delete');


//     Route::get('/category/all', [CategoryController::class, 'getCategoriesDatatable'])->name('category.all');
//     Route::post('/category/delete', [CategoryController::class, 'delete'])->name('category.delete');



//     Route::get('/posts/all', [PostsController::class, 'getPostsDatatable'])->name('posts.all');
//     Route::post('/posts/delete', [PostsController::class, 'delete'])->name('posts.delete');


//     Route::resources([
//         'users' => UserController::class,
//         'category' => CategoryController::class,
//         'posts' => PostsController::class,
//     ]);
// });

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::get('/', function () {
//     return view('admin.index');
// });

// Route::get('/app', function () {
//     return view('admin.app');
// });


// Route::get('/gest', function () {
//     return view('admin.gest');
// });

// Route::get('/login', function () {
//     return view('admin.auth.login');
// });


// Route::get('/register', function () {
//     return view('admin.auth.register');
// });


// Route::get('/forgot', function () {
//     return view('admin.auth.forgot');
// });


// Route::get('/reset', function () {
//     return view('admin.auth.reset');
// });


// Route::get('/verify', function () {
//     return view('admin.auth.verify');
// }); 