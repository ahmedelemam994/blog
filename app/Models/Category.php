<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Staudenmeir\EloquentEagerLimit\HasEagerLimit;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Category extends Model implements  TranslatableContract
{
    use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;

    use Translatable , HasEagerLimit , HasFactory , SoftDeletes;


    public $translatedAttributes = ['title', 'content'];
    protected $fillable = [ 'id', 'created_at', 'updated_at','deleted_at'];

    public function posts()
    {
       return $this->hasMany(Post::class);
    }

}
