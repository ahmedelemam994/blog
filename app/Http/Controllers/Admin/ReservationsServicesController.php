<?php
 namespace App\Http\Controllers\Admin;
 use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ReservationService;

class ReservationsServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the ReservationService
        $AllData = ReservationService::all()->paginate(2);
        // load the view and pass the ReservationService
        return view('admin.ReservationService.index', compact('AllData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.ReservationService.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'mobile'=>'required'
        ]);

        $ReservationService = new ReservationService();
        $ReservationService->name = $request->name;
        $ReservationService->email = $request->email;
        $ReservationService->mobile = $request->mobile;
        $ReservationService->detail = $request->detail;
        $save = $ReservationService->save();

        if( $save ){
            return redirect('reservations-services')->with('fail','Something went Wrong, failed to register');

        }else{
            return redirect()->back()->with('fail','Something went Wrong, failed to register');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ReservationService = ReservationService::find($id);
        return view('admin.ReservationService.show', compact('ReservationService'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ReservationService = ReservationService::find($id);
        return view('admin.ReservationService.edit', compact('ReservationService'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'mobile'=>'required'
        ]);
        $ReservationService = ReservationService::find($id);
        $ReservationService->name = $request->name;
        $ReservationService->email = $request->email;
        $ReservationService->mobile = $request->mobile;
        $ReservationService->detail = $request->detail;
        $save = $ReservationService->save();

        if( $save ){
            return redirect('reservations-services')->with('fail','Something went Wrong, failed to register');

        }else{
            return redirect()->back()->with('fail','Something went Wrong, failed to register');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ReservationService = ReservationService::find($id);
        $ReservationService->delete();

        // redirect
        return redirect('reservations-services');
    }
}
