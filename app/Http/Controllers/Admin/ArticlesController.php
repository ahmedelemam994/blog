<?php

namespace App\Http\Controllers\Admin;
use App\Models\Article;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticlesController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $Articles = Article::all();
        return view('admin.articles.index', compact('Articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categories = Category::all();
        return view('admin.articles.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $data = Article::create($request->all());     
        if($request->file('image')){
            $file= $request->file('image');
            $filename= date('YmdHi'). $file->getClientOriginalName();
            $file-> move(public_path('public/Image'), $filename);
            $data['image']= $filename;
        }

        $data['article_date'] = $request->article_date;
        $data->save();   
        return redirect('articles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Article = Article::find($id);
        return view('admin.Article.show', compact('Article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $categories = Category::all();
        $Article = Article::find($id);
        return view('admin.articles.edit', compact('categories','Article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $Article = Article::find($id);
        if($request->file('image')){
            $file= $request->file('image');
            $filename= date('YmdHi'). $file->getClientOriginalName();
            $file-> move(public_path('public/Image'), $filename);
            $Article['image']= $filename;
        }

        $Article['article_date'] = $request->article_date;
        $Article->update($request->all());
        return redirect('articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Article = Article::find($id);
        $Article->delete();

        // redirect
        return redirect('articles');
    }
}
